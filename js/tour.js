(function(){
	var tour = new Tour({
		storage: false
	});
	tour.addSteps(
		[
		{
			element: '.tour-step.tour-step-one',
			placement: 'bottom',
			title: 'Welcome to edureka!',
			content: 'We offer live classes!'
		},
		{
			element: '.tour-step.tour-step-two',
			placement: 'bottom',
			title: 'Welcome to edureka!',
			content: 'You can change your batch anytime!'
		},
		{
			element: '.tour-step.tour-step-three',
			placement: 'bottom',
			title: 'Welcome to edureka!',
			content: 'We provide customer support 24x7x365 !!'
		},
		{
			orphan: true,
			backdrop: true,
			placement: 'top',
			title: 'We are done',
			content: 'finally!! Hope you enjoyed my site!!'
		}
		]
	);
	tour.init();
	tour.start();
}());
